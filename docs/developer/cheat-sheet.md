# Developer Cheat Sheet

## Test

Validate the specification.

```bash
commands/validate.sh
```

## Lint

Check all files meet standards. This command runs locally all linters that
will run in the pipeline on a push.

```bash
commands/lint.sh
```

## Squash commits to prepare for merge into main

Before merging a merge request, use the following command to squash its
commits into a single commit, writing a good conventional-commit message.

```bash
commands/premerge-squash.sh
```
