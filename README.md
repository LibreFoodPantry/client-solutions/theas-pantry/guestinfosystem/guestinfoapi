# Guest Info System API Specification

The GuestInfoAPI provides an OpenAPI specification that is implemented
by the GuestInfoBackend and called by the GuestInfoFrontend. The developers
of the Backend and the Frontend are the clients of GuestInfoAPI.

## Client Guide

* [View or download the latest GuestInfoSystemAPI specification](specification/openapi.yaml)

Each specification file is written in [OpenAPI](https://www.openapis.org/),
a well-adopted, standardized language based on [YAML](https://yaml.org/).
There are many 3rd-party tools that work with OpenAPI specifications that
may be useful to frontend or backend development. Here are some references
to get you started.

* [OpenAPI Tools](https://openapi.tools/)
* [The Swagger Viewer extension for VS Code](
  https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer)
  renders the specification locally.
* GitLab provides a nice view of OpenAPI specification so long as it is
  stored in a single file named `openapi.yaml`.

## Developer Guide

Getting Started

### Working Locally

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/)
    and join its Discord server.
2. [Install development environment](docs/developer/install-development-environment.md)
3. Clone this repository using the following command

    ```bash
    git clone <repository-clone-url>
    ```

4. Open it in VS Code and reopen it in a devcontainer.
5. [specification/](specification/) contains
[specification/openapi.yaml](specification/openapi.yaml).
6. Familiarize yourself with the systems used by this project
  (see Development Infrastructure below).
7. See [the developer cheat-sheet](docs/developer/cheat-sheet.md) for common
  commands you'll likely use.

### Working with [Gitpod](https://www.gitpod.io/)

You can do your development work in the cloud with Gitpod. To use this project
in Gitpod, open [https://gitpod.io/#https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfoapi](https://gitpod.io/#https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfoapi)

It may take a little while for all the recommended VSCode extensions to be
installed.

**When you finish your work, be sure to stop your Gitpod workspace. Click
the orange Gitpod button in the lower-left, and choose
`Gitpod: Stop workspace`.**

### Development Infrastructure

* [Automated Testing](docs/developer/automated-testing.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
